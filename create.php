<?php
include __DIR__ . "/templates/header.php";
require "common.php";
?>

<?php
if(isset($_POST['submit']) && $_POST['submit'] === "Submit") {

    require "config.php";

    try {
        $connection = new PDO( $dsn, $username, $password, $options );
        // insert new user code

        $new_user = array(
            'firstname' => $_POST['firstname'],
            'lastname' => $_POST['lastname'],
            'email' => $_POST['email'],
            'age' => $_POST['age'],
            'location' => $_POST['location']
        );

        $sql = sprintf(
            "INSERT INTO %s (%s) VALUES (%s)",
            "user",
            implode(", ", array_keys($new_user)),
            ":" . implode(", :", array_keys($new_user))
        );

        $statement = $connection->prepare($sql);
        $statement->execute($new_user);
    
    } catch (PDOException $e) {
        echo $sql . "<br><br>" . $e->getMessage();
    }

}
?>

<h2>Add a user</h2>

<?php if($statement) { ?>
<p> <?php echo escape($_POST['firstname']); ?> successfully added. </p>
<?php } ?>

<form method="POST">
    <label for="firstname">First Name</label>
    <input type="text" name="firstname" id="firstname">
    <label for="lastname">Last Name</label>
    <input type="text" name="lastname" id="lastname">
    <label for="email">Email Address</label>
    <input type="text" name="email" id="email">
    <label for="age">Age</label>
    <input type="text" name="age" id="age">
    <label for="location">Location</label>
    <input type="text" name="location" id="location">
    <input type="submit" name="submit" value="Submit">
</form>

<a href="index.php">Back to home</a>

<?php include __DIR__ . "/templates/footer.php"; ?>