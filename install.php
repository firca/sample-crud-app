<?php

require "config.php";

try {
    $connection = new PDO( $dsn, $username, $password, $options );
    $sql = file_get_contents("data/init.sql");
    $connection->exec($sql);

    echo "Database and tabble users created successfuly";
} catch (PDOException $e) {
    echo $sql . "<br><br>" . $e->getMessage();
}