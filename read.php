<?php include __DIR__ . "/templates/header.php"; ?>

<?php
if(isset($_POST['submit'])) {
    try {
        require 'config.php';
        require 'common.php';

        $connection = new PDO($dsn, $username, $password, $options);
        // Fetch data code

        $sql = "SELECT * FROM user WHERE location = :location";
        $location = $_POST['location'];
        $statement = $connection->prepare($sql);
        $statement->bindParam(':location', $location, PDO::PARAM_STR);
        $statement->execute();
        
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);

        // echo '<pre>';
        // var_dump($result);
        // echo '</pre>';

    } catch (PDOException $e) {
        echo $sql . "<br><br>" . $e->getMessage();
    }
}
?>

<h2>Find user based on location</h2>

<?php
if(isset($_POST['submit'])){
    if($result && $statement->rowCount() > 0){
        ?>
            <h2>Results</h2>
            <table>
                <thead>
                    <tr>
                        <td>#</td>
                        <td>First Name</td>
                        <td>Last Name</td>
                        <td>Email Address</td>
                        <td>Age</td>
                        <td>Location</td>
                        <td>Date</td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($result as $row) { ?>
                        <tr>
                            <td> <?php echo $row['id'] ?> </td>
                            <td> <?php echo $row['firstname'] ?> </td>
                            <td> <?php echo $row['lastname'] ?> </td>
                            <td> <?php echo $row['email'] ?> </td>
                            <td> <?php echo $row['age'] ?> </td>
                            <td> <?php echo $row['location'] ?> </td>
                            <td> <?php echo $row['date'] ?> </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php
    }
    else {
        echo 'No results found for : ' . escape($_POST['location']);
    }
}
?>

<form method="POST">
    <label for="location">Location</label>
    <input type="text" name="location" required id="location">
    <input type="submit" name="submit" value="Submit">
</form>



<a href="index.php">Back to home</a>

<?php include __DIR__ . "/templates/footer.php"; ?>